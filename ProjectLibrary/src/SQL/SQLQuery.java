/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author user
 */
public abstract class SQLQuery {
    
    // <editor-fold defaultstate="collapsed" desc="Không cần quan tâm">
    // <editor-fold defaultstate="" desc="Query Constant">
    // Constants
    protected static final String SQLQUERYKEYADD = "add ";
    protected static final String SQLQUERYKEYFROM = "from ";
    protected static final String SQLQUERYKEYAS = "as ";
    protected static final String SQLQUERYKEYAVG = "avg ";
    protected static final String SQLQUERYKEYBETWEEN = "between ";
    protected static final String SQLQUERYKEYCREATETABLE = "create table ";
    protected static final String SQLQUERYKEYGROUPBY = "group by ";
    protected static final String SQLQUERYKEYLIKE = "like ";
    protected static final String SQLQUERYKEYLIMIT = "limit ";
    protected static final String SQLQUERYKEYMAX = "max ";
    protected static final String SQLQUERYKEYMIN = "min ";
    protected static final String SQLQUERYKEYROUND = "round ";
    protected static final String SQLQUERYKEYSUM = "sum ";
    protected static final String SQLQUERYKEYSET = "set ";
    protected static final String SQLQUERYKEYINNERJOIN = "inner join ";
    protected static final String SQLQUERYKEYVALUES = "values ";
    
    // <editor-fold defaultstate="Collapsed" desc="Các toán tử của lệnh SQL">
    protected static final String SQLQUERYKEYIN = "in ";
    protected static final String SQLQUERYKEYAND = "and ";
    protected static final String SQLQUERYKEYOR = "or ";
    
    // </editor-fold>
    
    // Query Where part
    protected static final String SQLQUERYKEYWHERE = "where ";
    
    // Query OrderBy part
    protected static final String SQLQUERYKEYORDERBY = "order by ";
    protected static final String SQLQUERYKEYORDERBYASC = "asc ";
    protected static final String SQLQUERYKEYORDERBYDESC = "desc ";
    
    // <editor-fold defaultstate="Collapsed" desc="Query Type">
    // QueryType
    public static final String SQLQUERYTYPESELECT = "Select";
    public static final String SQLQUERYTYPEDELETE = "Delete";
    public static final String SQLQUERYTYPEINSERT = "Insert";
    public static final String SQLQUERYTYPEUPDATE = "Update";
    // </editor-fold>
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table's Thing">
    // TableID
    public static final String SQLTABLEHOADON = "HoaDon";
    public static final String SQLTABLELIBRARIAN = "Librarian";
    public static final String SQLTABLEREADER = "Reader";
    public static final String SQLTABLEBOOK = "BookTable";
    public static final String SQLTABLEBORROWDETAIL = "BorrowDetails";
    public static final String SQLTABLECONFIG = "ProgramConfig";
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Bảng Hóa đơn">
    public static final String SQLTABLEHOADONIDHOADON = "ID_HoaDon";
    public static final String SQLTABLEHOADONIDREADER = "ID_Reader";
    public static final String SQLTABLEHOADONIDLIBRARIAN = "ID_Librarian";
    public static final String SQLTABLEHOADONTIENDATCOC = "Deposit";
    public static final String SQLTABLEHOADONTIENPHAT = "ReturnLateMoney";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Bảng Thủ thư">
    public static final String SQLTABLELIBRARIANID = "ID_Librarian";
    public static final String SQLTABLELIBRARIANTEN = "Librarian_Name";
    public static final String SQLTABLELIBRARIANDIACHI = "Address";
    public static final String SQLTABLELIBRARIANSODIENTHOAI = "Phone_Number";
    public static final String SQLTABLELIBRARIANUSERNAME = "Username";
    public static final String SQLTABLELIBRARIANPASSWORD = "Password";
    public static final String SQLTABLELIBRARIANADMIN = "Admin";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Bảng Độc giả">
    public static final String SQLTABLEREADERID = "ID_Reader";
    public static final String SQLTABLEREADERTEN = "Name_Reader";
    public static final String SQLTABLEREADERSOCMT = "SoCMT";
    public static final String SQLTABLEREADERSODIENTHOAI = "Phone_Number";
    public static final String SQLTABLEREADERDIACHI = "Address";
    public static final String SQLTABLEREADERMAIL = "Mail";
    public static final String SQLTABLEREADERNGAYLAP = "Create_Date";
    public static final String SQLTABLEREADERNGAYHETHAN = "Expiration_Date";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Bảng Sách">
    public static final String SQLTABLEBOOKID = "ID_sach";
    public static final String SQLTABLEBOOKTEN = "TenSach";
    public static final String SQLTABLEBOOKTHELOAI = "TheLoai";
    public static final String SQLTABLEBOOKSOLUONG = "SoLuong";
    public static final String SQLTABLEBOOKTACGIA = "TacGia";
    public static final String SQLTABLEBOOKNHAXUATBAN = "NhaXuatBan";
    public static final String SQLTABLEBOOKNAMXUATBAN = "NamXuatBan";
    public static final String SQLTABLEBOOKGHICHU = "GhiChu";
    public static final String SQLTABLEBOOKSOTIEN = "SoTien";
    public static final String SQLTABLEBOOKTINHTRANG = "TinhTrangSach";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Bảng Mượn trả">
    public static final String SQLTABLEBORROWDETAILIDHOADON = "ID_HoaDon";
    public static final String SQLTABLEBORROWDETAILIDSACH = "ID_sach";
    public static final String SQLTABLEBORROWDETAILNGAYMUON = "Borrow_Date";
    public static final String SQLTABLEBORROWDETAILNGAYTRA = "Return_Date";
    public static final String SQLTABLEBORROWDETAILHANTRA = "Return_Deadline";
    public static final String SQLTABLEBORROWDETAILGHICHU = "Comment";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="TableCols for Config">
    public static final String SQLTABLECONFIGRETURNDAY = "DayToReturnBook";
    public static final String SQLTABLECONFIGLASTUSERNAME = "LastLoginUsername";
    public static final String SQLTABLECONFIGLASTPASSWORD = "LastLoginPassword";
    public static final String SQLTABLECONFIGPENALTYMONEY = "PenaltyMoney";
    // </editor-fold>
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Thuộc tính của SQLQuery">
    
    // <editor-fold defaultstate="collapsed" desc="Thuộc tính tự thêm">
    // Properties
    protected String queryType;
    protected String queryStatement;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Các lựa chọn thêm vào của các câu lệnh SQL">
    // Optional Parts of query
    public HashMap dictWhereField;
    public boolean order;
    public String orderType;
    
    // </editor-fold>
    // </editor-fold>
    
    protected SQLQuery(String queryType) {
        this.queryType = queryType;
    }
    
    public abstract String statementFromQuery();
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Thêm các thuộc tính cho các phần của câu lệnh SQL - Dùng cẩn thận">
    public void setOrder(boolean order, String orderType) {
        this.order = order;
        this.orderType = orderType;
    }
    
    public void setWhere(HashMap<String, Vector<String>> dictWhereField) {
        this.dictWhereField = dictWhereField;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Parse statements parts(where fields, blah blah blah)">
    public void statementWithParts() {
        // <editor-fold defaultstate="collapsed" desc="Where Part">
        if(dictWhereField != null) {
            this.queryStatement = this.queryStatement.concat(" " + SQLQUERYKEYWHERE);
            Set setKeys = this.dictWhereField.keySet();
            for (Object strKey : setKeys) {
                this.queryStatement = this.queryStatement.concat((String)strKey + " ");
                this.queryStatement = this.queryStatement.concat(SQLQUERYKEYIN + "(");
                for(Object strKeyValue : (Vector)this.dictWhereField.get(strKey)) {
                    if (strKeyValue != ((Vector)this.dictWhereField.get(strKey)).lastElement()) {
                        this.queryStatement = this.queryStatement.concat("'" + (String)strKeyValue + "',");
                    } else {
                        this.queryStatement = this.queryStatement.concat("'" + (String)strKeyValue + "'");
                    }
                }
                this.queryStatement = this.queryStatement.concat(")");
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Order By Part">
        if(order) {
            this.queryStatement = this.queryStatement.concat(SQLQUERYKEYORDERBY);
            this.queryStatement = this.queryStatement.concat(orderType);
        }
        // </editor-fold>
    }
    // </editor-fold>
    
}
