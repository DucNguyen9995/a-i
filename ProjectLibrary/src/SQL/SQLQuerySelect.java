/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.Vector;

/**
 *
 * @author user
 */
public class SQLQuerySelect extends SQLQuery {
    
    // <editor-fold defaultstate="collapsed" desc="Key & Properties">
    private static final String SQLQUERYKEYDISTINCT = "distinct ";
    private static final String SQLQUERYKEYCOUNT = "count ";
    private static final String SQLQUERYKEYSELECT = "select ";
    
    private Vector columns;
    private Vector tables;
    // </editor-fold>
    
    private SQLQuerySelect(String queryType) {
        super(queryType);
    }
    
    public static SQLQuerySelect queryWithVectorColumns(Vector columns, Vector tables) {
        SQLQuerySelect querySelect = new SQLQuerySelect(SQLQUERYTYPESELECT);
        querySelect.columns = columns;
        querySelect.tables = tables;
        return querySelect;
    }
    
    @Override
    public String statementFromQuery() {
        String statementQuery = SQLQUERYKEYSELECT;
        if (this.columns == null) {
            statementQuery = statementQuery.concat("* ");
        } else {
            for (Object strColumns : this.columns) {
                if (strColumns == this.columns.lastElement()) {
                    statementQuery = statementQuery.concat((String)strColumns + " ");
                    break;
                }
                statementQuery = statementQuery.concat((String)strColumns + ", ");
            }
            
        }
        statementQuery = statementQuery.concat(SQLQUERYKEYFROM);
        if (this.tables == null){
            System.out.println("Table Empty is not allowed");
            return null;
        } else {
            for (Object strTable : this.tables) {
                if (strTable == this.tables.lastElement()) {
                    statementQuery = statementQuery.concat((String)strTable + " ");
                    break;
                }
                statementQuery = statementQuery.concat((String)strTable + " " + SQLQUERYKEYINNERJOIN + " ");
            }
        }
        this.statementWithParts();
        return statementQuery;
    }
}
