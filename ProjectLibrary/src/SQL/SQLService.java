/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class SQLService {
    
    private static final SQLService INSTANCE = new SQLService();
    private static final String SQLDATABASELINK = "jdbc:odbc:Library";
    
    // <editor-fold defaultstate="collapsed" desc="Connection & Test">
    private SQLService() {
        try {
            DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
        } catch (Exception e) {
            
        }        
    }
    
    public static SQLService getInstance() {
        return SQLService.INSTANCE;
    }
    
    public Connection connectToDatabase() {
        try {
            return DriverManager.getConnection(SQLDATABASELINK);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Failed To Connect To Database.\nPlease contact the Database manager for more info.");
            System.exit(1);
        }
        return null;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Các hàm gửi request">
    public ResultSet executeQuerySelectWithQuery(SQLQuery query) {
        String statementQuery;
        statementQuery = query.statementFromQuery();
        ResultSet resultSet = null;
        try {
            Statement statement = this.connectToDatabase().createStatement();
            if (query instanceof SQLQuerySelect)
                resultSet = statement.executeQuery(statementQuery);
            else {
                statement.executeQuery(statementQuery);
            }
            return resultSet;
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultSet;
    }
    
    public ResultSet executeQuerySelectWithSQLCommand(String sqlCmd) {
        try {
            return this.connectToDatabase().createStatement().executeQuery(sqlCmd);
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int executeQueryWithQuery(SQLQuery query) {
        String statementQuery;
        statementQuery = query.statementFromQuery();
        try {
            Statement statement = this.connectToDatabase().createStatement();
            int notError = statement.executeUpdate(statementQuery);
            return notError;
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public void executeQueryWithSQLCommand(String sql) {
        try {
            this.connectToDatabase().createStatement().executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeUpdateQueryWithBatch(final Vector vectBatch) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Statement statementBatch = connectToDatabase().createStatement();
                    for (Object object : vectBatch) {
                        statementBatch.addBatch((String)object);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }
    // </editor-fold>
}
