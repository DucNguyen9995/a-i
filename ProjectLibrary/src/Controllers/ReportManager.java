/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import java.util.Vector;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class ReportManager {
    
    public static ReportManager getInstance() {
        return ReportManagerHolder.INSTANCE;
    }
    
    private static class ReportManagerHolder {
        private static final ReportManager INSTANCE = new ReportManager();
    }
    
    public void reportWithContextAndTable(String tblName, HashMap hashMapContext, Vector vectKeySet, JTable table) {
        JFileChooser fileChooser = new JFileChooser();
        int i = fileChooser.showSaveDialog(null);
        if (i == JFileChooser.APPROVE_OPTION) {
            Document document = new Document();
            File file = fileChooser.getSelectedFile();
            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file + ".pdf"));
                document.open();
                
                Font timeNewRomans = new Font(BaseFont.createFont("C:\\Users\\user\\Documents\\NetBeansProjects\\ProjectLibrary\\ProjectLibrary\\resources\\times_0.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                timeNewRomans.setSize(24);
                
                Font tahoma = new Font(BaseFont.createFont("C:\\Users\\user\\Documents\\NetBeansProjects\\ProjectLibrary\\ProjectLibrary\\resources\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
                tahoma.setSize(30);
                
                Paragraph para = new Paragraph("Trường Đại Học Bách Khoa Hà Nội\nThư Viện Tạ Quang Bửu", tahoma);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                
                timeNewRomans.setSize(15);
                timeNewRomans.setStyle(Font.UNDERLINE);
                
                para = new Paragraph("http://library.hust.edu.vn/ ", timeNewRomans);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                
                timeNewRomans.setStyle(Font.NORMAL);
                
                para = new Paragraph("Địa chỉ: Tạ Quang Bửu, Bách Khoa, Hai Bà Trưng, Hà Nội\n\n\n", timeNewRomans);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                
                para = new Paragraph(tblName + "\n\n", tahoma);
                para.setAlignment(Element.ALIGN_CENTER);
                document.add(para);
                
                if (hashMapContext != null) {
                    PdfPTable pdfTableContext = new PdfPTable(5);
                    pdfTableContext.setWidthPercentage(100);
                    float[] colWidths = {1f,1f,1f,1f,1f};
                    pdfTableContext.setWidths(colWidths);
                    
                    for (Object object : vectKeySet) {
                        tahoma.setSize(16);
                        tahoma.setStyle(Font.NORMAL);
                        PdfPCell cellKey = new PdfPCell(new Paragraph((String)object + ": ", tahoma));
                        cellKey.setBorderColor(BaseColor.WHITE);
                        cellKey.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cellKey.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        PdfPCell cellValue = new PdfPCell(new Paragraph("  " + (String)hashMapContext.get(object), tahoma));
                        cellValue.setBorderColor(BaseColor.WHITE);
                        cellValue.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cellValue.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        PdfPCell cellNull = new PdfPCell();
                        cellNull.setBorderColor(BaseColor.WHITE);
                        
                        pdfTableContext.addCell(cellKey);
                        pdfTableContext.addCell(cellValue);
                        pdfTableContext.addCell(cellNull);
                        pdfTableContext.addCell(cellNull);
                        pdfTableContext.addCell(cellNull);
                    }
                    document.add(pdfTableContext);
                    
                    tahoma.setStyle(Font.NORMAL);
                    tahoma.setSize(18);
                    document.add(new Paragraph("Chi tiết mượn:", tahoma));
                }
                
                document.add(new Paragraph("\n"));
                
                PdfPTable tableResult = new PdfPTable(table.getColumnCount());
                float[] colWidthsTableResult = new float[table.getColumnCount()];
                for (int j = 0; j < table.getColumnCount(); j++) {
                    colWidthsTableResult[j] = 1f;
                }
                tableResult.setWidthPercentage(110);
                tableResult.setWidths(colWidthsTableResult);
                for (int col = 0; col < table.getModel().getColumnCount(); col++) {
                    timeNewRomans.setSize(12);
                    timeNewRomans.setStyle(Font.NORMAL);
                    timeNewRomans.setStyle(Font.BOLD);
                    PdfPCell cellHeader = new PdfPCell(new Paragraph(table.getModel().getColumnName(col), timeNewRomans));
                    cellHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellHeader.setBackgroundColor(BaseColor.MAGENTA);
                    
                    tableResult.addCell(cellHeader);
                }
                for (int row = 0; row < table.getModel().getRowCount(); row++) {
                    for (int col = 0; col < table.getModel().getColumnCount(); col++) {
                        timeNewRomans.setStyle(Font.NORMAL);
                        PdfPCell cellInfo = new PdfPCell(new Paragraph((String)table.getModel().getValueAt(row, col), timeNewRomans));
                        cellInfo.setHorizontalAlignment(Element.ALIGN_CENTER);
                        
                        tableResult.addCell(cellInfo);
                    }
                }
                document.add(tableResult);
                
                document.add(new Paragraph("\n"));
                
                // Ending
                PdfPTable pdfTable = new PdfPTable(2);
                pdfTable.setWidthPercentage(100);
                float[] colWidths = {1f,1f};
                pdfTable.setWidths(colWidths);

                timeNewRomans.setSize(13);
                timeNewRomans.setStyle(Font.BOLD);
                PdfPCell cellNull = new PdfPCell();
                cellNull.setBorderColor(BaseColor.WHITE);
                String date = new SimpleDateFormat("yyy-MM-dd").format(new java.sql.Date(new java.util.Date().getTime()));
                String[] str = date.split("\\-");
                para = new Paragraph("Hà Nội, Ngày " + str[2] + " Tháng " + str[1] + " Năm " + str[0] + "\nNgười Lập\n\n\n\n" + AppController.getInstance().currentLibrarianName, timeNewRomans);
                para.setAlignment(Paragraph.ALIGN_CENTER);
                PdfPCell cellEnding = new PdfPCell(para);
                cellEnding.setHorizontalAlignment(Element.ALIGN_CENTER);
                cellEnding.setBorderColor(BaseColor.WHITE);

                pdfTable.addCell(cellNull);
                pdfTable.addCell(cellEnding);
                
                document.add(pdfTable);
                
                
                // Close to prevent leak memory
                document.close();
                writer.close();
                JOptionPane.showMessageDialog(null, "Saved Succesful!");
            } catch (IOException | DocumentException ex) {
                JOptionPane.showMessageDialog(null, "Save Failed!");
                Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
