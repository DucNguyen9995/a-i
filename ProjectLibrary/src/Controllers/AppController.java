/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import SQL.*;
import SQL.SQLService;
import Views.AppMainWindow;
import Views.CreateNewAccountBox;
import Views.LoginWindow;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author user
 */
public class AppController {
    
    public static final AppController APPCONTROLLER = getInstance();
    public static boolean isRunning;
    public String currentLibrarianID;
    public String currentLibrarianName;
    public boolean adminPrivilege;
    public int configDayToReturnBook;
    public int configPenaltyPerBook;
    
    private AppController() {
        AppController.isRunning = true;
        this.bootUp();
    }
    
    public static AppController getInstance() {
        return AppControllerHolder.INSTANCE;
    }
    
    private static class AppControllerHolder {
        private static final AppController INSTANCE = new AppController();
    }
    
    public void bootUp() {
        this.currentLibrarianID = "";
        this.currentLibrarianName = "";
        Vector vectTables = new Vector();
        vectTables.add(SQLQuery.SQLTABLELIBRARIAN);
        SQLQuery query = SQLQuerySelect.queryWithVectorColumns(null, vectTables);
        try {
            if (query instanceof SQLQuerySelect) {
                ResultSet resultSet = SQLService.getInstance().executeQuerySelectWithQuery(query);
                if (!resultSet.next()) {
                    CreateNewAccountBox firstBox = new CreateNewAccountBox(null);
                } else {
                    LoginWindow login = new LoginWindow();
                }
            }
        } catch (SQLException ex) {
            
        }
    }
    
    public void accessGranted() {
        AppMainWindow appMainWindow = new AppMainWindow("Damn");
    }
}
