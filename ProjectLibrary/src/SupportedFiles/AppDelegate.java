/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupportedFiles;

import SQL.SQLQuery;
import SQL.SQLQuerySelect;
import SQL.SQLService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author user
 */
public abstract class AppDelegate {
    
    public static String autoGenerateIDForTable(String table, String tableColumnID) {
        String uniqueID = UUID.randomUUID().toString();
        uniqueID = uniqueID.replace("-", "");
        uniqueID = uniqueID.substring(0, 9);
        if (table == null && tableColumnID == null)
            return uniqueID;
        Vector vectTable = new Vector();
        vectTable.add(table);
        SQLQuery query = SQLQuerySelect.queryWithVectorColumns(null, vectTable);
        
        ResultSet resultSet = SQL.SQLService.getInstance().executeQuerySelectWithQuery(query);
        try {
            while (resultSet.next()) {
                if (resultSet.getString(tableColumnID).equals(uniqueID)) {
                    uniqueID = UUID.randomUUID().toString();
                    uniqueID = uniqueID.replace("-", "");
                    uniqueID = uniqueID.substring(0, 9);
                }
            }
            return uniqueID;
        } catch (SQLException ex) {
            Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uniqueID;
    }
    
    /**
     * Use when ReloadTable 
     * @param vectTables
     * @param vectColumns
     * @return tableData
     */
    public static DefaultTableModel reloadTableWithVectTables(Vector vectTables, Vector vectColumns) {
        SQLQuery query = SQLQuerySelect.queryWithVectorColumns(vectColumns , vectTables);
        ResultSet rs = SQLService.getInstance().executeQuerySelectWithQuery(query);
        return reloadTableWithResultSet(rs);
    }
    
    public static DefaultTableModel reloadTableWithVectTables(Vector vectTables) {
        return reloadTableWithVectTables(vectTables, null);
    }
    
    public static String reparsePassword(String pass) {
        pass = pass.replace(",", "");
        pass = pass.replace("[", "");
        pass = pass.replace(" ", "");
        return pass.replace("]", "");
    }
    
    public static DefaultTableModel reloadTableWithResultSet(final ResultSet rs) {
        try {
            DefaultTableModel tblData = new DefaultTableModel() {
                @Override
                public int getColumnCount() {
                    try {
                        return rs.getMetaData().getColumnCount();
                    } catch (SQLException ex) {
                        Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return 0;
                }
                
                @Override
                public String getColumnName(int column) {
                    try {
                        return rs.getMetaData().getColumnName(column + 1).replaceAll("_", " ");
                    } catch (SQLException ex) {
                        Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }
                
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            while (rs.next()) {
                Vector vectRowData = new Vector();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    if (rs.getMetaData().getColumnType(i) == Types.DATE) {
                        vectRowData.add(rs.getDate(i));
                        continue;
                    }
                    vectRowData.add(rs.getString(i));
                }
                tblData.addRow(vectRowData);
            }
            return tblData;
        } catch(SQLException ex) {
            JOptionPane.showMessageDialog(null, "Reload Data Failed");
            Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void reloadTableWithSQLCmd(String sqlCmd, JTable table) {
        try {
            table.setModel(reloadTableWithResultSet(SQLService.getInstance().connectToDatabase().createStatement().executeQuery(sqlCmd)));
        } catch (SQLException ex) {
            Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, "reloadTableWithSQLCmd", ex);
        }
    }
    
    public static void reloadTableWithPreparedStatement(PreparedStatement st, JTable table) {
        try {
            table.setModel(reloadTableWithResultSet(st.executeQuery()));
        } catch (SQLException ex) {
            Logger.getLogger(AppDelegate.class.getName()).log(Level.SEVERE, "reloadTableWithPreparedStatement", ex);
        }
    }
}
