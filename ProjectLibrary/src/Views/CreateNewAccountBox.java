/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controllers.AppController;
import SQL.SQLQuery;
import SQL.SQLService;
import SupportedFiles.AppDelegate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class CreateNewAccountBox extends javax.swing.JDialog {

    public AppMainWindow owner;
    private Vector vectFields;
    /**
     * Creates new form FirstTimeLoginBox
     */
    public CreateNewAccountBox(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        vectFields = new Vector();
        vectFields.add(fieldName);
        vectFields.add(fieldPhoneNumber);
        vectFields.add(fieldAddress);
        vectFields.add(fieldUsername);
        vectFields.add(fieldPassword);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblSignUp = new javax.swing.JLabel();
        lblLibrarianName = new javax.swing.JLabel();
        fieldName = new javax.swing.JTextField();
        lblPhoneNumber = new javax.swing.JLabel();
        fieldPhoneNumber = new javax.swing.JTextField();
        lblAddress = new javax.swing.JLabel();
        fieldAddress = new javax.swing.JTextField();
        lblUsername = new javax.swing.JLabel();
        fieldUsername = new javax.swing.JTextField();
        lblPassword = new javax.swing.JLabel();
        fieldPassword = new javax.swing.JPasswordField();
        lblConfirmPassword = new javax.swing.JLabel();
        fieldConfirmPassword = new javax.swing.JPasswordField();
        lblWarning = new javax.swing.JLabel();
        btnCreate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblSignUp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblSignUp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSignUp.setText("New Account");
        lblSignUp.setVerifyInputWhenFocusTarget(false);

        lblLibrarianName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLibrarianName.setText("* Librarian Name:");

        fieldName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NameActionPerformed(evt);
            }
        });

        lblPhoneNumber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPhoneNumber.setText("Phone Number:");

        fieldPhoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldPhoneNumberActionPerformed(evt);
            }
        });

        lblAddress.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAddress.setText("Address:");

        fieldAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldAddressActionPerformed(evt);
            }
        });

        lblUsername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsername.setText("* Username:");

        fieldUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldUsernameActionPerformed(evt);
            }
        });

        lblPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPassword.setText("* Password:");
        lblPassword.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        fieldPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldPasswordActionPerformed(evt);
            }
        });

        lblConfirmPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblConfirmPassword.setText("* Confirm Password");
        lblConfirmPassword.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        fieldConfirmPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldConfirmPasswordActionPerformed(evt);
            }
        });

        lblWarning.setForeground(new java.awt.Color(255, 0, 0));
        lblWarning.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblWarning.setText("Wrong in field Confirm Password");
        lblWarning.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateDidTap(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelDidTap(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblUsername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(fieldUsername)
                                    .addComponent(fieldPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lblAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(fieldAddress))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lblLibrarianName, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(fieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lblPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(fieldPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(lblSignUp, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(lblWarning, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblConfirmPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fieldConfirmPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSignUp, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLibrarianName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPhoneNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldConfirmPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblConfirmPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblWarning)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(btnCancel))
                .addContainerGap())
        );

        fieldName.getAccessibleContext().setAccessibleName("NameField");
        fieldPhoneNumber.getAccessibleContext().setAccessibleName("PhoneNumberField");
        fieldAddress.getAccessibleContext().setAccessibleName("AddressField");
        fieldUsername.getAccessibleContext().setAccessibleName("UsernameField");
        fieldPassword.getAccessibleContext().setAccessibleName("PasswordFIeld");
        fieldConfirmPassword.getAccessibleContext().setAccessibleName("ConfirmPasswordField");
        btnCreate.getAccessibleContext().setAccessibleName("LoginButton");
        btnCancel.getAccessibleContext().setAccessibleName("CancelButton");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelDidTap(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelDidTap
        // TODO add your handling code here:
        if(this.owner instanceof AppMainWindow)
            this.dispose();
        else
            System.exit(0);
    }//GEN-LAST:event_btnCancelDidTap

    private void fieldPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldPasswordActionPerformed
        // TODO add your handling code here:
        fieldConfirmPassword.requestFocus();
    }//GEN-LAST:event_fieldPasswordActionPerformed

    private void fieldUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldUsernameActionPerformed
        // TODO add your handling code here:
        fieldPassword.requestFocus();
    }//GEN-LAST:event_fieldUsernameActionPerformed

    private void btnCreateDidTap(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateDidTap
        // TODO add your handling code here:
        if (!(AppDelegate.reparsePassword(Arrays.toString(fieldPassword.getPassword())).equals(AppDelegate.reparsePassword(Arrays.toString(fieldConfirmPassword.getPassword()))))) {
            this.lblWarning.setText("Password not match!");
            this.lblWarning.setVisible(true);
            return;
        }
        if ("".equals(fieldName.getText()) || "".equals(fieldUsername.getText()) || "".equals(Arrays.toString(fieldPassword.getPassword())) || "".equals(Arrays.toString(fieldConfirmPassword.getPassword()))) {
            this.lblWarning.setText("Required field is blank!");
            this.lblWarning.setVisible(true);
            return;
        }
        
        try {
            // Create Query
            PreparedStatement statement = SQLService.getInstance().connectToDatabase().prepareStatement("select " + SQLQuery.SQLTABLELIBRARIANUSERNAME + " from " + SQLQuery.SQLTABLELIBRARIAN);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                if (rs.getString(1).equals(this.fieldUsername.getText())) {
                    this.lblWarning.setText("Username existed");
                    this.lblWarning.setVisible(true);
                    return;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateNewAccountBox.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String sqlQueryCmd = "insert into " + SQLQuery.SQLTABLELIBRARIAN + "(";
            sqlQueryCmd = sqlQueryCmd.concat(SQLQuery.SQLTABLELIBRARIANID + "," + SQLQuery.SQLTABLELIBRARIANTEN + ",");
            if (!"".equals(fieldPhoneNumber.getText()))
                sqlQueryCmd = sqlQueryCmd.concat(SQLQuery.SQLTABLELIBRARIANSODIENTHOAI + ",");
            if (!"".equals(fieldAddress.getText()))
                sqlQueryCmd = sqlQueryCmd.concat(SQLQuery.SQLTABLELIBRARIANDIACHI + ",");
            sqlQueryCmd = sqlQueryCmd.concat(SQLQuery.SQLTABLELIBRARIANUSERNAME + "," + SQLQuery.SQLTABLELIBRARIANPASSWORD + "," + SQLQuery.SQLTABLELIBRARIANADMIN);
            sqlQueryCmd = sqlQueryCmd.concat(") values (?,");
            for (Object object : vectFields) {
                if (!"".equals(((JTextField)object).getText())) {
                    if (object == vectFields.lastElement())
                        sqlQueryCmd = sqlQueryCmd.concat("?,?)");
                    else 
                        sqlQueryCmd = sqlQueryCmd.concat("?,");
                }
            }
            PreparedStatement statement = SQLService.getInstance().connectToDatabase().prepareStatement(sqlQueryCmd);
            int i = 1;
            statement.setString(i, AppDelegate.autoGenerateIDForTable(SQLQuery.SQLTABLELIBRARIAN, SQLQuery.SQLTABLELIBRARIANID));
            i++;
            for (Object object : vectFields) {
                if (!"".equals(((JTextField)object).getText()))
                    statement.setString(i++, ((JTextField)object).getText());
                    if (object == vectFields.lastElement()) {
                        if (!(this.owner instanceof AppMainWindow))
                            statement.setBoolean(i++, true);
                        else
                            statement.setBoolean(i++, false);
                    }
            }    
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CreateNewAccountBox.class.getName()).log(Level.SEVERE, null, ex);
        }
        AppController.APPCONTROLLER.accessGranted();
        this.dispose();
    }//GEN-LAST:event_btnCreateDidTap

    private void NameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NameActionPerformed
        // TODO add your handling code here:
        fieldPhoneNumber.requestFocus();
    }//GEN-LAST:event_NameActionPerformed

    private void fieldPhoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldPhoneNumberActionPerformed
        // TODO add your handling code here:
        fieldAddress.requestFocus();
    }//GEN-LAST:event_fieldPhoneNumberActionPerformed

    private void fieldAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldAddressActionPerformed
        // TODO add your handling code here:
        fieldUsername.requestFocus();
    }//GEN-LAST:event_fieldAddressActionPerformed

    private void fieldConfirmPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldConfirmPasswordActionPerformed
        // TODO add your handling code here:
        if (!Arrays.toString(this.fieldPassword.getPassword()).equals(Arrays.toString(this.fieldConfirmPassword.getPassword()))) {
            this.lblWarning.setText("Password not match!");
            this.lblWarning.setVisible(true);
        } else {
            this.lblWarning.setVisible(false);
        }
        
        this.btnCreateDidTap(evt);
    }//GEN-LAST:event_fieldConfirmPasswordActionPerformed

    public CreateNewAccountBox(final AppMainWindow owner) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CreateNewAccountBox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CreateNewAccountBox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CreateNewAccountBox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CreateNewAccountBox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        this.owner = owner;
        
        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                final CreateNewAccountBox dialog;
                if (owner == null)
                    dialog = new CreateNewAccountBox(new javax.swing.JFrame(), true);
                else 
                    dialog = new CreateNewAccountBox(owner, true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        if (owner == null)
                            System.exit(0);
                        dialog.dispose();
                    }
                });
                dialog.lblWarning.setVisible(false);
                dialog.setVisible(true);
            }
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCreate;
    private javax.swing.JTextField fieldAddress;
    private javax.swing.JPasswordField fieldConfirmPassword;
    private javax.swing.JTextField fieldName;
    private javax.swing.JPasswordField fieldPassword;
    private javax.swing.JTextField fieldPhoneNumber;
    private javax.swing.JTextField fieldUsername;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblConfirmPassword;
    private javax.swing.JLabel lblLibrarianName;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPhoneNumber;
    private javax.swing.JLabel lblSignUp;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JLabel lblWarning;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>
}
